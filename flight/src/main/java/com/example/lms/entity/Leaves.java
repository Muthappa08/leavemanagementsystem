package com.example.lms.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Leaves {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long leaveId;
	private Long userId;
	private Long sickleaves;
	private Long flexiLeaves;
	public Long getLeaveId() {
		return leaveId;
	}
	public void setLeaveId(Long leaveId) {
		this.leaveId = leaveId;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getSickleaves() {
		return sickleaves;
	}
	public void setSickleaves(Long sickleaves) {
		this.sickleaves = sickleaves;
	}
	public Long getFlexiLeaves() {
		return flexiLeaves;
	}
	public void setFlexiLeaves(Long flexiLeaves) {
		this.flexiLeaves = flexiLeaves;
	}
	@Override
	public String toString() {
		return "Leaves [leaveId=" + leaveId + ", userId=" + userId + ", sickleaves=" + sickleaves + ", flexiLeaves="
				+ flexiLeaves + "]";
	}


	
}
