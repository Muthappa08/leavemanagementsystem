package com.example.lms.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.lms.entity.LeavesTransactions;

public interface LmsRepository extends JpaRepository<LeavesTransactions,Long>{

	Optional<List<LeavesTransactions>> findByUserId(Long userId);
}
