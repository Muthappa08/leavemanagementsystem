package com.example.lms.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.lms.entity.Leaves;

public interface UserRepository extends JpaRepository<Leaves, Long> {

	Optional<Leaves> findByUserId(Long userId);

}
