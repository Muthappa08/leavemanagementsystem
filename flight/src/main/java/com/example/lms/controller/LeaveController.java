package com.example.lms.controller;

import java.text.ParseException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.lms.dto.LmsTransactionDto;
import com.example.lms.dto.ResponseDto;
import com.example.lms.entity.LeavesTransactions;
import com.example.lms.serviceImpl.LeavesTransactionsServicesImpl;

@RestController
@RequestMapping("/lms")
public class LeaveController {

	@Autowired
	LeavesTransactionsServicesImpl leavesTransactionsServicesImpl;
	
	@PostMapping("/leavetransaction")
	public  ResponseEntity<Optional<ResponseDto>> applyLeave(@RequestBody LmsTransactionDto lmsTransactionDto ) throws ParseException{
		Optional<ResponseDto> responseDto=leavesTransactionsServicesImpl.applyLeave(lmsTransactionDto);
		return new ResponseEntity<>(responseDto, HttpStatus.OK);
	}
}
