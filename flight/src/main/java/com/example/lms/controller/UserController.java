package com.example.lms.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.lms.dto.LeavesDto;
import com.example.lms.dto.LmsTransactionDto;
import com.example.lms.entity.Leaves;
import com.example.lms.serviceImpl.UserServicesImpl;



@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	UserServicesImpl userServicesImpl;
	
	@GetMapping("/{userId}/employeeleaves")
	public  ResponseEntity<Optional<LeavesDto>> userLogin(@PathVariable("userId")Long userId ){
		Optional<LeavesDto> leaves=userServicesImpl.userLogin(userId);
		return new ResponseEntity<>(leaves, HttpStatus.OK);
	}
	
	@GetMapping("/{userId}/leavetransaction")
	public  ResponseEntity<Optional<List<LmsTransactionDto>>> leavesHistory(@PathVariable("userId")Long userId ){
		Optional<List<LmsTransactionDto>> lmsTransactionDto=userServicesImpl.leavesHistory(userId);
		return new ResponseEntity<>(lmsTransactionDto, HttpStatus.OK);
	}
}
