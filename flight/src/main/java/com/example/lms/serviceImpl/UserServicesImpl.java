package com.example.lms.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.example.lms.constants.ApplicationConstants;
import com.example.lms.dto.LeavesDto;
import com.example.lms.dto.LmsTransactionDto;
import com.example.lms.entity.Leaves;
import com.example.lms.entity.LeavesTransactions;
import com.example.lms.exception.InvalidUserException;
import com.example.lms.repository.LmsRepository;
import com.example.lms.repository.UserRepository;
import com.example.lms.service.UserServices;

@Service
public class UserServicesImpl implements UserServices {

	@Autowired
	UserRepository userRepository;
	@Autowired
	LmsRepository lmsRepository;
	ModelMapper mapper = new ModelMapper();
	public Optional<LeavesDto> userLogin(Long userId) {
		Optional<Leaves> leaves=userRepository.findByUserId(userId);
		if(!leaves.isPresent()) {
			throw new InvalidUserException(ApplicationConstants.INVALID_USER);
		}
		LeavesDto leavesDto=new LeavesDto();
		leavesDto.setFlexiLeaves(leaves.get().getFlexiLeaves());
		leavesDto.setSickleaves(leaves.get().getSickleaves());
		return Optional.of(leavesDto);
	}
	
	
	public Optional<List<LmsTransactionDto>> leavesHistory(Long userId) {
		Optional<List<LeavesTransactions>> list=lmsRepository.findByUserId(userId);
		if(!list.isPresent()) {
			throw new InvalidUserException(ApplicationConstants.INVALID_USER);
		}
	List <LmsTransactionDto>list2=new ArrayList();
	     List<LeavesTransactions> LeaveTransactionList = list.get();
	     List<LmsTransactionDto> LeaveTransactionDtoList = new ArrayList();
	     for(LeavesTransactions ltx:LeaveTransactionList) {
	    	 //CustomerOrderResponseDto customerOrderResponseDto =  mapper.map(customerOrder, CustomerOrderResponseDto.class);
	    	 LmsTransactionDto lmsTransactionDto=mapper.map(ltx, LmsTransactionDto.class);
	    	 LeaveTransactionDtoList.add(lmsTransactionDto);
	     }

		return Optional.of(LeaveTransactionDtoList);
	}

}
