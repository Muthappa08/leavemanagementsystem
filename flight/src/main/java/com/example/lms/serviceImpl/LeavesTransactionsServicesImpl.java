package com.example.lms.serviceImpl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.lms.constants.ApplicationConstants;
import com.example.lms.dto.LmsTransactionDto;
import com.example.lms.dto.ResponseDto;
import com.example.lms.entity.Leaves;
import com.example.lms.entity.LeavesTransactions;
import com.example.lms.exception.InvalidUserException;
import com.example.lms.exception.LeavesAreLess;
import com.example.lms.repository.LmsRepository;
import com.example.lms.repository.UserRepository;
import com.example.lms.service.LeavesTransactionsServices;


@Service
public class LeavesTransactionsServicesImpl implements LeavesTransactionsServices{

	@Autowired
	UserRepository userRepository;
	@Autowired
	LmsRepository lmsRepository;
	
	public Optional<ResponseDto> applyLeave(LmsTransactionDto lmsTransactionDto) throws ParseException {
		
		Optional<Leaves> leaves=userRepository.findByUserId(lmsTransactionDto.getUserId());
		if(!leaves.isPresent()) {
			throw new InvalidUserException(ApplicationConstants.INVALID_USER);
		}
		String leaveType=lmsTransactionDto.getLeaveType().toString();

		String dateStr = lmsTransactionDto.getFromDate().toString();
		DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
		Date date = (Date)formatter.parse(dateStr);
		LocalDate localDate1 = Instant.ofEpochMilli(date.getTime())  
				.atZone(ZoneId.systemDefault())
				.toLocalDate();
		
		String dateStr2 = lmsTransactionDto.getToDate().toString();
		DateFormat formatter2 = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
		Date date2 = (Date)formatter.parse(dateStr2);
		LocalDate localDate2 = Instant.ofEpochMilli(date2.getTime())
				.atZone(ZoneId.systemDefault())
				.toLocalDate();
		
		long noOfDaysBetween = ChronoUnit.DAYS.between(localDate1,localDate2 );
		noOfDaysBetween=noOfDaysBetween+1;
		if(leaveType.equalsIgnoreCase("sickleave")) {
			
		if(leaves.get().getSickleaves()>noOfDaysBetween) {
			leaves.get().setSickleaves(leaves.get().getSickleaves()-noOfDaysBetween);
			Leaves leaves2=new Leaves();
			BeanUtils.copyProperties(leaves.get().getClass(), leaves2);
			userRepository.save(leaves2);
			LeavesTransactions txn=new LeavesTransactions();
			BeanUtils.copyProperties(lmsTransactionDto, txn);
			lmsRepository.save(txn);
		}else
			throw new LeavesAreLess(ApplicationConstants.LEAVES_ARE_LESS);
		}
		if(leaveType.equalsIgnoreCase("flexileave")) {
			if(leaves.get().getFlexiLeaves()>noOfDaysBetween) {
				leaves.get().setFlexiLeaves((leaves.get().getFlexiLeaves()-noOfDaysBetween));
				Leaves leaves2=new Leaves();
				BeanUtils.copyProperties(leaves.get().getClass(), leaves2);
				userRepository.save(leaves2);
				LeavesTransactions txn=new LeavesTransactions();
				BeanUtils.copyProperties(lmsTransactionDto, txn);
				lmsRepository.save(txn);
			}else
				throw new LeavesAreLess(ApplicationConstants.LEAVES_ARE_LESS);
		}
		ResponseDto responseDto =new ResponseDto(); 
		responseDto.setStatusCode(ApplicationConstants.LEAVES_APPLIED_CODE);
		responseDto.setMessage(ApplicationConstants.LEAVES_APPLIED);
		return Optional.of(responseDto);
	}

	
}
