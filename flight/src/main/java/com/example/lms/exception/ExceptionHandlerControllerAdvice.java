
package com.example.lms.exception;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.example.lms.constants.ApplicationConstants;

@RestControllerAdvice
public class ExceptionHandlerControllerAdvice {

	@ExceptionHandler(Exception.class)
	public ExceptionResponse handleException(final Exception exception, final HttpServletRequest request) {

		ExceptionResponse error = new ExceptionResponse();
		error.setErrorMessage(exception.getMessage());
		error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		return error; 
	}  
	@ExceptionHandler(InvalidUserException.class)
	public ExceptionResponse handleInvalidUser(final InvalidUserException exception, final HttpServletRequest request) {

		ExceptionResponse error = new ExceptionResponse();
		error.setErrorMessage(exception.getMessage());
		error.setStatus(ApplicationConstants.INVALID_USER_CODE);
		return error; 
	}  
	@ExceptionHandler(LeavesAreLess.class)
	public ExceptionResponse handleLeavesless(final LeavesAreLess exception, final HttpServletRequest request) {

		ExceptionResponse error = new ExceptionResponse();
		error.setErrorMessage(exception.getMessage());
		error.setStatus(ApplicationConstants.INVALID_USER_CODE);
		return error; 
	}  

}
