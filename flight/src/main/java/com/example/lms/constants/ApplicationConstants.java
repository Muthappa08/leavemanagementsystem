package com.example.lms.constants;

public class ApplicationConstants {

private ApplicationConstants() {
		
	}

	public static final Integer SUCCESS_CODE = 200;
	
			
	
	public static final Integer INVALID_USER_CODE = 600;
	public static final String INVALID_USER = "Invalid user";
	
	public static final Integer LEAVES_ARE_LESS_CODE = 601;  
	public static final String LEAVES_ARE_LESS = "Leaves are less";
	
	public static final Integer LEAVES_APPLIED_CODE = 602;  
	public static final String LEAVES_APPLIED = "Leaves applied successfully";
}
