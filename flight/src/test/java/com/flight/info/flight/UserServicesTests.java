package com.flight.info.flight;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.example.lms.constants.ApplicationConstants;
import com.example.lms.dto.LeavesDto;
import com.example.lms.dto.LmsTransactionDto;
import com.example.lms.dto.ResponseDto;
import com.example.lms.entity.Leaves;
import com.example.lms.entity.LeavesTransactions;
import com.example.lms.exception.InvalidUserException;
import com.example.lms.repository.LmsRepository;
import com.example.lms.repository.UserRepository;
import com.example.lms.serviceImpl.LeavesTransactionsServicesImpl;
import com.example.lms.serviceImpl.UserServicesImpl;

@RunWith(MockitoJUnitRunner.Silent.class)
public class UserServicesTests {

	@InjectMocks
	UserServicesImpl userServicesImpl;
	@Mock
	LmsRepository lmsRepository;
	@Mock
	UserRepository userRepository;
	@InjectMocks
	LeavesTransactionsServicesImpl leavesTransactionsServicesImpl;
	@Test
	public void userLogin() {
		Leaves leave=new Leaves();
		leave.setUserId((long) 123);
		leave.setLeaveId((long) 1);
		leave.setFlexiLeaves((long) 20);
		leave.setSickleaves((long) 10);
		Mockito.when(userRepository.findByUserId((long) 123)).thenReturn(Optional.of(leave));
		LeavesDto leavesDto=new LeavesDto();
		leavesDto.setFlexiLeaves((long) 20);
		leavesDto.setSickleaves((long) 10);
		Optional<LeavesDto> leavesDto2=userServicesImpl.userLogin((long) 123);
		assertEquals(leavesDto2.get().getFlexiLeaves(),leavesDto.getFlexiLeaves() );
		
	}
	
	/*
	 * @Test(expected=InvalidUserException.class) public void UserLoginwithInvalid()
	 * { Leaves leave=new Leaves(); Mockito.when(userRepository.findByUserId((long)
	 * 123)).thenReturn(Mockito.any()); Optional<LeavesDto>
	 * leavesDto2=userServicesImpl.userLogin((long) 123); assertNotNull(leavesDto2);
	 * }
	 */
	@Test
	public void userHistory() {
		List<LeavesTransactions> list=new ArrayList();
		LeavesTransactions leavesTransactions=new LeavesTransactions();
		leavesTransactions.setLeaveStatus("Approved");
		leavesTransactions.setReason("not well");
		leavesTransactions.setUserId(1L);
		list.add(leavesTransactions);
		Mockito.when(lmsRepository.findByUserId(1L)).thenReturn(Optional.of(list));
		Optional<List<LmsTransactionDto>> list2= userServicesImpl.leavesHistory(1L);
		assertEquals(list2.get().size(), list.size());
				
	}
	
	@Test
	public void applyLeavePositive() throws ParseException {
		Leaves leave = new Leaves();
		leave.setUserId((long) 123);
		leave.setLeaveId((long) 1);
		leave.setFlexiLeaves((long) 20);
		leave.setSickleaves((long) 10);
		Mockito.when(userRepository.findByUserId(1L)).thenReturn(Optional.of(leave));
		LmsTransactionDto lmsTransactionDto = new LmsTransactionDto();
		lmsTransactionDto.setLeaveStatus("approved");
		  String sDate1="31/12/1998";
		  Date date1=new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);
		lmsTransactionDto.setFromDate(date1);
		lmsTransactionDto.setToDate(date1);
		lmsTransactionDto.setUserId(1L);
		lmsTransactionDto.setLeaveType("sickleave");
		Optional<ResponseDto> response=leavesTransactionsServicesImpl.applyLeave(lmsTransactionDto); //
		assertEquals(response.get().getMessage(), ApplicationConstants.LEAVES_APPLIED);
	 
	  }
	@Test
	public void applyLeavePositive2() throws ParseException {
		Leaves leave = new Leaves();
		leave.setUserId((long) 123);
		leave.setLeaveId((long) 1);
		leave.setFlexiLeaves((long) 20);
		leave.setSickleaves((long) 10);
		Mockito.when(userRepository.findByUserId(1L)).thenReturn(Optional.of(leave));
		LmsTransactionDto lmsTransactionDto = new LmsTransactionDto();
		lmsTransactionDto.setLeaveStatus("approved");
		  String sDate1="31/12/1998";
		  Date date1=new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);
		lmsTransactionDto.setFromDate(date1);
		lmsTransactionDto.setToDate(date1);
		lmsTransactionDto.setUserId(1L);
		lmsTransactionDto.setLeaveType("flexileave");
		Optional<ResponseDto> response=leavesTransactionsServicesImpl.applyLeave(lmsTransactionDto); //
		assertEquals(response.get().getMessage(), ApplicationConstants.LEAVES_APPLIED);
	 
	  }
}
